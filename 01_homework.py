import scrapy
import re
from scrapy.http    import Request

class BlogSpider(scrapy.Spider):
    name = 'DDWbot'
    DOWNLOAD_DELAY = 0.2
    start_urls = ['https://www.csfd.cz/']
    allowed_domains = ['csfd.cz']

    def parse(self, response):
        self.log('I just visited: ' + response.url)

        #Url pattern of film page
        filmPattern = re.compile(".*csfd\.cz\/film\/.*")

        if filmPattern.match(response.url):
            yield {
                'title': response.xpath('//h1[@itemprop="name"]/text()').extract_first(),
                'rating': response.css('h2.average::text').extract_first(),
                'genre': response.css('p.genre::text').extract_first(),
                'origin': response.css('p.origin::text').extract_first(),
                'date': response.xpath('//span[@itemprop="dateCreated"]/text()').extract_first()
            }

        #follow links
        links = response.xpath('//a/@href').extract()

        # We stored already crawled links in this list
        crawledLinks = []

        #Relative path pattern
        linkPattern = re.compile("^\/")

        for link in links:
            # If it is a proper link and is not checked yet, yield it to the Spider
            if linkPattern.match(link) and not link in crawledLinks:
                link = response.urljoin(link)
                crawledLinks.append(link)
                yield Request(link, self.parse)
